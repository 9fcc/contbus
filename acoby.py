import pathlib
import subprocess


class Config:
    def __init__(self, distr, codename, arch, data_path, source_path=None, build_path=None, container_source_path=None, container_build_path=None):
        self.data_path = data_path
        self.source_path = source_path

        if not build_path:
            build_path = '.'
        build_path += '/acoby/' + distr + '/' + codename + '/' + arch
        self.build_path = build_path

        self.distr = distr
        self.codename = codename
        self.arch = arch

        if container_source_path is None:
            container_source_path = '/source'
        self.container_source_path = container_source_path

        if container_build_path is None:
            container_build_path = '/build'
        self.container_build_path = container_build_path


class Container:
    def __init__(self, config, registry=None, id=None):
        self.config = config
        self.name = config.distr + '/' + config.codename + ':' + config.arch
        self.registry = registry
        if registry is not None:
            self.registry_path = registry + '/' + self.name
        else:
            self.registry_path = self.name
        if id is not None:
            self.registry_path += '-' + id
        self.id = id
        self.prepare()

    def prepare(self):
        p = pathlib.Path(self.config.data_path, 'platform',
                         self.config.distr, self.config.codename, self.config.arch)
        if not p.exists():
            p = pathlib.Path(self.config.data_path,
                             'platform', self.config.distr)
        self.platform_path = p
        self.platform_file = p / 'Dockerfile'
        if not self.platform_file.exists():
            with self.platform_file.open('w', encoding="utf-8") as f:
                f.write('FROM ' + self.config.arch + '/' +
                        self.config.distr + ':' + self.config.codename + '\n')

    def pull(self):
        if self.registry is not None:
            return subprocess.run(['docker', 'pull', self.registry_path])

    def push(self):
        if self.registry is not None:
            return subprocess.run(['docker', 'push', self.registry_path])

    def build(self):
        command = [
            'docker', 'build',
            '--build-arg', 'SOURCE_DIR='+self.config.container_source_path,
            '--build-arg', 'BUILD_DIR='+self.config.container_build_path,
            '--build-arg', 'DISTR='+self.config.distr,
            '--build-arg', 'CODENAME='+self.config.codename,
            '--build-arg', 'ARCH='+self.config.arch,
            '-t', self.registry_path,
            self.platform_path
        ]
        p = subprocess.run(command)
        return p

    def run(self, command=None):
        run_args = [
            'docker', 'run',
            '-v', self.config.source_path + ":" + self.config.container_source_path,
            '-v', self.config.build_path + ":" + self.config.container_build_path,
            self.registry_path
        ]
        if command is not None:
            if isinstance(command, list):
                run_args += command
            else:
                run_args += [command]
        p = subprocess.run(run_args)
        return p
