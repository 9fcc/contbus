import argparse
from acoby import *

parser = argparse.ArgumentParser()
parser.add_argument(
    '--data-path', help='Path to Acoby data directory', required=True)
parser.add_argument('--build-path', help='Path to the build directory')
parser.add_argument('--arch', help='Architecture')
parser.add_argument('--distr', help='Operating system distributive')
parser.add_argument('--codename', help='Distributive codename')
parser.add_argument('--registry', help='Docker hub registry')
parser.add_argument('--id', help='Image id tag')
parser.add_argument(
    '--push', action=argparse.BooleanOptionalAction, help='Docker hub registry')
args = parser.parse_args()

config = Config(
    args.distr,
    args.codename,
    args.arch,
    args.data_path,
    build_path=args.build_path)

container = Container(config, args.registry, args.id)
p = container.build()
if p.returncode:
    exit(p.returncode)

if args.push:
    p = container.push()
    exit(p.returncode)
