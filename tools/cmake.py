import subprocess
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    '--source-path', help='Path to source directory', default='/source')
parser.add_argument(
    '--build-path', help='Path to build directory', default='/build')
parser.add_argument(
    "--package", action=argparse.BooleanOptionalAction, default=False)
parser.add_argument(
    "--test", action=argparse.BooleanOptionalAction, default=False)
parser.add_argument("--generators")
parser.add_argument("gen_args", nargs="*")
args = parser.parse_args()

cmake_args = ['cmake', '-S',  args.source_path,
              '-B',  args.build_path]
if args.gen_args is not None:
    cmake_args += args.gen_args
subprocess.run(cmake_args)

cmake_args = ['cmake', '--build', args.build_path]
p = subprocess.run(cmake_args)
if p.returncode:
    exit(p.returncode)

if args.test:
    ctest_args = ['ctest']
    p = subprocess.run(ctest_args, cwd=args.build_path)
    if p.returncode:
        exit(p.returncode)

if args.package:
    cpack_args = ['cpack']
    if args.generators is not None:
        cpack_args += ['-G', args.generators]
    p = subprocess.run(cpack_args, cwd=args.build_path)
    if p.returncode:
        exit(p.returncode)
