from pathlib import Path
import argparse
from acoby import *

parser = argparse.ArgumentParser()
parser.add_argument(
    '--data-path', help='Path to Acoby data directory', required=True)
parser.add_argument('--source-path', help='Path to the source directory')
parser.add_argument('--build-path', help='Path to the build directory')
parser.add_argument('--container-source-path',
                    help='Path to the source directory inside container')
parser.add_argument('--container-build-path',
                    help='Path to the build directory inside container')
parser.add_argument('--arch', help='Architecture')
parser.add_argument('--distr', help='Operating system distributive')
parser.add_argument('--codename', help='Distributive codename')
parser.add_argument('--registry', help='Docker hub registry')
parser.add_argument('--id', help='Image id tag')
parser.add_argument('--build', action=argparse.BooleanOptionalAction,
                    default=False, help='Image id tag')
parser.add_argument(
    '--tool', help='Name of the python script that will be executed (without .py)')
args, user_args = parser.parse_known_args()

config = Config(
    args.distr,
    args.codename,
    args.arch,
    args.data_path,
    source_path=args.source_path,
    build_path=args.build_path,
    container_source_path=args.container_source_path,
    container_build_path=args.container_build_path)

if args.tool is not None:
    tool_path = config.data_path + '/tools' + args.tool + '.py'
    if not Path(tool_path).exists():
        tool_path = config.data_path + '/acoby/tools/' + args.tool + '.py'
    tool_path = config.container_source_path + '/' + \
        str(Path(tool_path).relative_to(config.source_path))
    script_args = [
        '--source-path', config.container_source_path,
        '--build-path', config.container_build_path
    ]
    user_args = ["python", tool_path] + script_args + user_args


container = Container(config, args.registry, args.id)
if args.build:
    p = container.build()
    if p.returncode:
        exit(p.returncode)
else:
    container.pull()
p = container.run(user_args)
exit(p.returncode)
